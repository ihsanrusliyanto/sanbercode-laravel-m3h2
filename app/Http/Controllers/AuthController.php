<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller {
    public function register(){
        return view('form');
    }

    public function getItem(Request $request){
        $firstName = $request->firstName;
        $lastName = $request->lastName;
        $gender = $request->gender;
        $nationality = $request->nationality;
        $language = [$request->bahasaIndonesia, $request->english, $request->otherLanguage];
        $bio = $request->bio;

        return view('welcome', compact('firstName', 'lastName'));
    }

    public function welcome(){
        return view('welcome');
    }
}