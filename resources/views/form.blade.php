<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>


    <form method="POST" action="/item">
        @csrf
        <h3>Sign Up Form</h3>

        <p><label for="firstName">First Name: </label></p>
        <input type="text" name="firstName"> <br>
        <p><label for="lastName">Last Name: </label></p>
        <input type="text" name="lastName"> <br>

        <p>Gender:</p>
        <input type="radio" name="gender" value="male">
        <label for="male">Male</label> <br>
        <input type="radio" name="gender" value="female">
        <label for="female">Female</label> <br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label> <br>

        <p><label for="nationality">Nationality: </label></p>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>

        <p>Languange Spoken: </p>
        <input type="checkbox" name="bahasaIndonesia">
        <label for="bahasaIndonesia">Bahasa Indonesia</label> <br>
        <input type="checkbox" name="english">
        <label for="english">English</label> <br>
        <input type="checkbox" name="otherLanguage">
        <label for="otherLanguage">Other</label> <br>

        <p>Bio:</p>
        <textarea name="bio" id="" cols="30" rows="10"></textarea> <br>

        <button type="submit">Sign Up</button>
    </form>
</body>
</html>